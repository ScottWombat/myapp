DROP TABLE IF EXISTS product_review;

CREATE TABLE product_review (
  id bigint(20) GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  productId bigint(20) NOT NULL,
  parentId bigint(20) DEFAULT NULL,
  title varchar(100) NOT NULL,
  rating smallint(6) NOT NULL DEFAULT 0,
  published tinyint(1) NOT NULL DEFAULT 0,
  createdAt timestamp NOT NULL,
  publishedAt timestamp DEFAULT NULL,
  content text,
  UNIQUE KEY idx_review_product (productId),
  UNIQUE KEY idx_review_parent (parentId)
)