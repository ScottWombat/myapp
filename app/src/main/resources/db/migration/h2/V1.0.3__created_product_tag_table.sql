DROP TABLE IF EXISTS product_tag;

CREATE TABLE product_tag (
  productId bigint(20) NOT NULL,
  tagId bigint(20) NOT NULL,
  PRIMARY KEY (productId,tagId),
  UNIQUE KEY idx_pt_tag (tagId),
  UNIQUE KEY idx_pt_product (productId)
) 