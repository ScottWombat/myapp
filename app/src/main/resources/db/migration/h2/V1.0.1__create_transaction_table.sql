DROP TABLE IF EXISTS transaction;

CREATE TABLE transaction (
  id bigint(20) GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  userId bigint(20) NOT NULL,
  orderId bigint(20) NOT NULL,
  code varchar(100) NOT NULL,
  type smallint(6) NOT NULL DEFAULT 0,
  mode smallint(6) NOT NULL DEFAULT 0,
  status smallint(6) NOT NULL DEFAULT 0,
  createdAt timestamp NOT NULL,
  updatedAt timestamp DEFAULT NULL,
  content text ,
  UNIQUE KEY idx_transaction_user (userId),
  UNIQUE KEY idx_transaction_order (orderId)
) ;