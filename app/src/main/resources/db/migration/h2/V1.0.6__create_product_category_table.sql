DROP TABLE IF EXISTS product_category;

CREATE TABLE product_category (
  productId bigint(20) NOT NULL,
  categoryId bigint(20) NOT NULL,
  PRIMARY KEY (productId,categoryId),
  UNIQUE KEY idx_pc_category (categoryId),
  UNIQUE KEY idx_pc_product (productId)
) ;